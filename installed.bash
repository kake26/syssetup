#!/bin/bash

# this code gets all user installed / updated packages, some are dependencies 

flist=installed.list

function installed() {

if [[ -f "$flist" ]] 
then
    echo "File $flist exists!"
    exit 0;
fi

ls /overlay/upper/usr/lib/opkg/info/*.list | sed -e 's/.*\///' | sed -e 's/\.list//' > installed.list

}