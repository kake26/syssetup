# syssetup

This system was created to allow of easy recreation or restoration of a existing system. This is not a backup application, nor does it create a image in the sense of a full disk image. It creates a snapshot of the current system configuration and installed packages. Once this information is collected it is packaged into standalone self extracting restore archive. It does this by copying your /etc folder and generating a list of installed programs via the package manager on the system. These are then packaged into executable you can copy and run on another system. Great for duplicating a say a raspberry pi configuration. Works well for VPS systems as well, especially when you just need to do a clean reinstall.

# Wiki

Yes, I'm setting up a wiki for this project. Its probably a glorified README, but for once I'll do it. Most of the README stuff will move there.

# Notes Last updated 5/31/24 check wiki for details

# Special Thanks 

To the fine folks that created codeium(Link below). The autosuggest / autocomplete features make things allot easier and save me a ton of typing. Having an AI that can function as an assistant is a wonderful thing. 

[![built with Codeium](https://codeium.com/badges/main)](https://codeium.com)