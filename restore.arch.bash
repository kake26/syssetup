#!/bin/bash

source ./restore.lib.bash

if [[ $? -ne 0 ]]; then
    echo "Failed to source restore.lib.bash"
    
    exit 1
fi

function cprint () { 
    color="$1"
    shift
    echo "$(tput setaf $color)$*$(tput sgr0)"

}

function mprint (){
    echo "    	$1  -  $2"
}

function incorrect_selection() {
  cprint 1 "Incorrect selection! Try again."
}

function press_enter() {
  echo ""
  cprint 3 "Press Enter to continue "
  read
  clear
}

function pacman() {
command pacman --noconfirm "$@"
}

function restore_config() {
  cprint 2 "Restoring system..."
  sleep 1
  aptpkgfile="installed_packages"
  # restore apt first
  cp -Rf etc/pacman.d /etc
  cp pacman.conf /etc
  pacman --noconfirm -Sy
  pacman --noconfirm -S rsync
    if [ -f ./exclusions ]; then
  exclusions # this runs exclusions
  fi
  pacman --noconfirm -S - < $aptpkgfile
  # we should be set app wise now the configs
  #cp -Rf etc/ /etc 
  rsync -avh --delete --progress etc/ /etc
  cprint 3 "You may want to reboot for changes to take effect"
  cprint 2 "Done!"
}

# Check for root only important on pure Debian as there is no sudo by default
if [ "$(id -u)" != "0" ]; then
    # check is the system is debian
    if [ -f /etc/debian_version ]; then
        cprint 3 "Notice: This script may need to be run as root on Debian, especially if its a stock install. Also requires makeself to created a distributable self restorable package" ; press_enter ;
    fi
fi

until [ "$selection" = "0" ]; do
clear
  echo ""
  cprint 3 "          Main Menu"
  mprint 1 "Restore"
  mprint 0 "Exit"
  echo ""
  echo -n "  Enter selection: "
  read selection
  echo ""
  case $selection in
    1 ) clear ; restore_config ; press_enter ;;
    2 ) clear ; menu_option_two ; press_enter ;;
    0 ) clear ; exit ;;
    * ) clear ; incorrect_selection ; press_enter ;;
  esac
done