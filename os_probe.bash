#!/bin/bash

# OS probing component

# Not an easy task to identify the distro, this app supports Debian based and Arch based
# Our best bet is to look for which package manager is installed

# check if apt-get is installed

echo "Checking OS.."

# step 1 check via os-release

if [ -f /etc/os-release ]; then
    . /etc/os-release
    if [ "$ID_LIKE" = "debian" ]; then
            tosp="Debian"
    elif [ "$ID_LIKE" = "arch" ]; then
        tosp="Arch"
    elif [ "$ID_LIKE" = "openwrt" ]; then
        tosp="Openwrt"
    fi
fi

if [ -x "$(command -v apt-get)" ]; then
    # set environment variable for os
     rosp="Debian"
fi

# check if pacman is installed

if [ -x "$(command -v pacman)" ]; then
    rosp="Arch"
fi

# check if opkg is installed

if [ -x "$(command -v opkg)" ]; then
    rosp="Openwrt"
fi

# I know its so high tech and amazing right?

# check /etc/os-release for the ID_LIKE value

if [[ "$tosp" == "$rosp" ]]; then
    echo "OS detection successful"
    osp=$tosp
else
    echo "Looks like $tosp, but based on the package manager its $rosp"
    osp=$rosp
fi

echo "Detected OS: $osp"

sleep 3


