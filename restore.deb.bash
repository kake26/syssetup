#!/bin/bash


source ./restore.lib.bash

if [[ $? -ne 0 ]]; then
    echo "Failed to source restore.lib.bash"
   
    exit 1
fi
  
export DEBIAN_FRONTEND=noninteractive
# we call cp directly and check for a cp alias
cpapp=$(which cp)

if alias -p | grep -q cp; then
  # we have a cp alias 
  unalias cp 
fi

# This should help prevent any funny business

function cprint () { 
    color="$1"
    shift
    echo "$(tput setaf $color)$*$(tput sgr0)"

}

function mprint (){
    echo "    	$1  -  $2"
}

function incorrect_selection() {
  cprint 1 "Incorrect selection! Try again."
}

function press_enter() {
  echo ""
  cprint 3 "Press Enter to continue "
  read
  clear
}

function restore_config() {
  cprint 2 "Restoring system..."
  sleep 1
  aptpkgfile="installed_packages"
  # restore apt first
  $cpapp -Rf etc/apt/ /etc
  chown -R _apt /etc/apt
  apt update
  apt full-upgrade -y
  apt install -y rsync
  # now the programs installed via apt before we restore configs
  if [ -f ./exclusions ]; then
  exclusions # this runs exclusions
  fi
  mkfast_deb # speed up this process

  # Novel idea but not good for errors: apt install -y $(cat $aptpkgfile | awk '{print $1}')
  apt install -y $(cat $aptpkgfile | awk '{print $1}')
  # check for error

  if [ $? -ne 0 ]; then
   cat installed_packages | awk '{print $1}' | while read -r package; do
    apt install -y $package
  done 
  fi
  
  # we should be set app wise now the configs
  #$cpapp -Rf etc/ /etc 
  rsync -avh --delete --progress etc/ /etc
  chown -R _apt /etc/apt
  cprint 3 "You may want to reboot for changes to take effect"
  cprint 2 "Done!"
}

# Check for root only important on pure Debian as there is no sudo by default
if [ "$(id -u)" != "0" ]; then
    # check is the system is debian
    if [ -f /etc/debian_version ]; then
        cprint 3 "Notice: This script may need to be run as root on Debian, especially if its a stock install. Also requires makeself to created a distributable self restorable package" ; press_enter ;
    fi
fi

until [ "$selection" = "0" ]; do
clear
  echo ""
  cprint 3 "          Main Menu"
  mprint 1 "Restore"
  mprint 0 "Exit"
  echo ""
  echo -n "  Enter selection: "
  read selection
  echo ""
  case $selection in
    1 ) clear ; restore_config ; press_enter ;;
    2 ) clear ; menu_option_two ; press_enter ;;
    0 ) clear ; exit ;;
    * ) clear ; incorrect_selection ; press_enter ;;
  esac
done