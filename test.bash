#!/bin/bash

aptpkgfile="installed_packages"

apt install -s -y $(cat $aptpkgfile | awk '{print $1}')
# check for error

if [ $? -ne 0 ]; then
   cat installed_packages | awk '{print $1}' | while read -r package; do
    apt install -q -s -y $package
done 
fi

#cat installed_packages | awk '{print $1}' | while read -r package; do
#    apt install -q -s -y $package
#done 

