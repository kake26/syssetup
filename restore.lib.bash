#!/bin/bash

pkgsum="pkgsum"
hostsum="hostsums"
hashapp="sha256sum"

# Extra functions for restore scripts

function exclusions () {
    # rebuild installed_packages file minus those listed in the exclusions file
    
    mv installed_packages installed_packages.orig
    grep -vf exclusions installed_packages.orig  > installed_packages

}

function mkfast_deb () {
    
    # we must check for packages already installed so we don't waste time reinstalling them

    dpkg --get-selections > installed_packages_fast
    grep -vf installed_packages_fast installed_packages.orig > installed_packages
    
}

function verify () {
find /etc -type f  -exec $hashapp '{}' \; > $hostsum

# I know this is a cheap trick, but it works

if [ -f $pkgsum ] && [ -f $hostsum ]; then
    diff -q $pkgsum $hostsum
    if [ $? -eq 0 ]; then
        echo "Verify passed"
    else
        echo "Verify failed"
    fi
else
    echo "Checksum files not found"
fi
}